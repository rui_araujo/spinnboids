#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "fixedptc.h"
#include "vec.h"
#include "boid.h"
#include "spinnaker.h"

uint random() {
	uint rng = sv->random;
	uint bit = (rng >> 31) ^ (rng >> 6) ^ (rng >> 5) ^ (rng >> 1);

	sv->random = rng = (rng << 1) | (bit & 1);

	return rng;
}

#define rrand(a) (random ()%a)

#define DEFAULT_CENTER_BIAS 7
#define DEFAULT_AVG_VEL     3
#define DEFAULT_CHILLING    1

Boid boids[NUMBOIDS];
Vec flocks_avgvel[16];
Vec big_flock_avgvel;
Vec real_center;

void new_boid(Boid * boid, int W, int H) {
	fixedpt px = 0, py = 0, pz = 0;
	fixedpt vx = 0, vy = 0, vz = 0;
	px = fixedpt_fromint(rrand((W<<4)) - (W << 3));
	py = fixedpt_fromint(rrand((H<<4)) - (H << 3));
	pz = fixedpt_fromint(rrand(2000) + 2000);
	set_vec(&(boid->pos), px, py, pz);

	vx = fixedpt_fromint(rrand(51) - 25);
	vy = fixedpt_fromint(rrand(51) - 25);
	vz = fixedpt_fromint(rrand(51) - 25);

	set_vec(&(boid->vel), vx, vy, vz);
}

/*
 * Move this boid, wrt allboids
 */

static Vec center;
static Vec center_bias;
static Vec avgvelocity;
static Vec avgvel_bias;
static Vec chilling;
void boid_move(Boid *boid, Boid allboids[], int numboids, Vec *real_center,
		Vec * real_avgvel) {

	if (boid->perching) {
		if (boid->perch_timer > 0) {
			boid->perch_timer--;
			return;
		} else {
			boid->perching = 0;
		}
	}
	boid_perceive_center(&center, boid, real_center, numboids);
	vec_diff(&center, &boid->pos, &center_bias);
	vec_rshift(&center_bias, DEFAULT_CENTER_BIAS);

	boid_av_vel(&avgvelocity, boid, real_avgvel, numboids);
	vec_diff(&avgvelocity, &boid->vel, &avgvel_bias);
	vec_rshift(&avgvel_bias, DEFAULT_AVG_VEL);
	vec_clear(&chilling);
	boid_chill_out(&chilling, boid, allboids, numboids);
	vec_rshift(&chilling, DEFAULT_CHILLING);

	vec_add(&boid->vel, &center_bias);
	vec_add(&boid->vel, &avgvel_bias);
	vec_add(&boid->vel, &chilling);

	vec_limit(&boid->vel, fixedpt_fromint(100));

	vec_add(&boid->pos, &boid->vel);

	/* bound world */
	if (boid->pos.x < fixedpt_fromint(-1800)) {
		boid->vel.x += fixedpt_fromint(10);
	}
	if (boid->pos.x > fixedpt_fromint(1800)) {
		boid->vel.x -= fixedpt_fromint(10);
	}
	if (boid->pos.y < fixedpt_fromint(-1200)) {
		boid->vel.y += fixedpt_fromint(10);
	}
	if (boid->pos.y > fixedpt_fromint(800)) {
		boid->vel.y -= fixedpt_fromint(10);
	}
	if (boid->pos.y > fixedpt_fromint(1000)) {
		boid->pos.y = fixedpt_fromint(1000); /* Hit ground!! */
		boid->vel.y = 0;
		boid->perching = 1;
		boid->perch_timer = (rrand(20) + 30);
	}
	if (boid->pos.z < fixedpt_fromint(3000)) {
		boid->vel.z += fixedpt_fromint(10);
	}
	if (boid->pos.z > fixedpt_fromint(5000)) {
		boid->vel.z -= fixedpt_fromint(10);
	}

}

void boid_perceive_center(Vec* perc_cent, Boid *boid, Vec *real_cent,
		int numboids) {
	vec_diff(real_cent, &boid->pos, perc_cent);
	vec_sdiv(perc_cent, fixedpt_fromint(numboids - 1));
}

void boid_av_vel(Vec *perc_avgvel, Boid *boid, Vec * real_avgvel, int numboids) {

	vec_diff(real_avgvel, &boid->vel, perc_avgvel);
	vec_sdiv(perc_avgvel, fixedpt_fromint(numboids - 1));

}
static Vec chill;
void boid_chill_out(Vec * bigchill, Boid *boid, Boid boids[], int numboids) {
	int i;
	vec_clear(bigchill);
	for (i = 0; i < numboids; i++) {
		if (boid != &boids[i]) {
			if (vec_rdist(&boid->pos, &(boids[i].pos)) <= fixedpt_fromint(100)) {
				vec_diff(&boid->pos, &(boids[i].pos), &chill);
				vec_add(bigchill, &chill);
			}
		}
	}

}

