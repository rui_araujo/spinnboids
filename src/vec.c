#include <math.h>
#include <stdlib.h>
#include "vec.h"
#include "spin1_api.h"

#define max(a,b) ((a>b)?a:b)

void set_vec(Vec * vec, fixedpt X, fixedpt Y, fixedpt Z) {
	vec->x = X;
	vec->y = Y;
	vec->z = Z;
}

void vec_clear(Vec * vec) {
	vec->x = 0;
	vec->y = 0;
	vec->z = 0;
}

void vec_diff(Vec * vec1, Vec * vec2, Vec * vec3) {
	vec3->x = vec1->x - vec2->x;
	vec3->y = vec1->y - vec2->y;
	vec3->z = vec1->z - vec2->z;
}

void vec_add(Vec * vec1, Vec * vec2) {
	vec1->x += vec2->x;
	vec1->y += vec2->y;
	vec1->z += vec2->z;
}

void vec_sdiv(Vec * vec, fixedpt scalar) {
	vec->x = fixedpt_div(vec->x, scalar);
	vec->y = fixedpt_div(vec->y, scalar);
	vec->z = fixedpt_div(vec->z, scalar);
}

void vec_smul(Vec * vec, fixedpt scalar) {
	vec->x = fixedpt_mul(vec->x, scalar);
	vec->y = fixedpt_mul(vec->y, scalar);
	vec->z = fixedpt_mul(vec->z, scalar);
}

void vec_rshift(Vec * vec, int n) {
	vec->x = vec->x >> n;
	vec->y = vec->y >> n;
	vec->z = vec->z >> n;
}

void vec_lshift(Vec * vec, int n) {
	vec->x = vec->x << n;
	vec->y = vec->y << n;
	vec->z = vec->z << n;
}

/*
 * Limit the length of the longest
 * component to lim, while keeping others
 * in proportion
 */
void vec_limit(Vec * vec, fixedpt lim) {
	fixedpt m, f;
	m = fixedpt_abs(vec->x);
	f = fixedpt_abs(vec->y);
	m = max(m, f);
	f = fixedpt_abs(vec->z);
	m = max(m, f);

	if (m <= lim)
		return;

	f = fixedpt_div(lim, m);
	vec_smul(vec, f);
}

/*
 * Rectangular (ie. min component) distance
 * between this and vec2
 */
fixedpt vec_rdist(Vec * vec1, Vec * vec2) {
	fixedpt dx, dy, dz, dm;

	dx = fixedpt_sub(vec1->x, vec2->x);
	dx = fixedpt_abs(dx);
	dy = fixedpt_sub(vec1->y, vec2->y);
	dy = fixedpt_abs(dy);
	dz = fixedpt_sub(vec1->z, vec2->z);
	dz = fixedpt_abs(dz);

	dm = max(dx, dy);
	dm = max(dm, dz);

	return dm;
}
