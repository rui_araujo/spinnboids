#ifndef __BOID_H__
#define __BOID_H__

#include "vec.h"

typedef struct {
	Vec pos;
	Vec vel;
	uint16_t perching; /* boolean */
	uint16_t perch_timer;
} Boid;

#define NUMBOIDS 150

extern Boid boids[NUMBOIDS];
extern Vec flocks_avgvel[16];
extern Vec big_flock_avgvel;
extern Vec real_center;

void new_boid(Boid * boid, int W, int H);
void boid_move(Boid *boid, Boid allboids[], int numboids, Vec *real_center,
		Vec * real_avgvel);
void boid_perceive_center(Vec* perc_cent, Boid *boid, Vec *real_cent,
		int numboids);
void boid_av_vel(Vec *perc_avgvel, Boid *boid, Vec * real_avgvel, int numboids);
void boid_chill_out(Vec * bigchill, Boid *boid, Boid boids[], int numboids);

#endif  /*  __BOID_H__  */
