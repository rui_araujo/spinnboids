/****h* Blinky/main.c
 *
 * SUMMARY
 * 	main file containing the c_main function
 *
 * AUTHOR
 * 	Matthias Meiner
 *
 * DETAILS
 * 	Created on		: 27.11.2012
 * 	Version			: 1.0
 * 	Last modified on	: 27.11.2012
 * 	Last modified by	: Matthias Meiner
 *
 * COPYRIGHT
 * 	Copuright (c) Technical University Munich, 2012-2013. All rights reserved.
 * 	Neuroscientific System Theory Group
 *
 *
 ******/

// SpiNNaker API
#include "spin1_api.h"
#include "spinn_io.h"
#include "spinn_sdp.h"

// C libraries
#include "stdint-gcc.h"

// Project headers
#include "boid.h"

static uint16_t coreID;
static uint32_t counter = 0;
void update_boids(uint ticks, uint null) {
	io_printf(IO_STD, "Count: %u\n",counter++);
	vec_clear(&real_center);
	vec_clear(&flocks_avgvel[coreID - 1]);

	for (uint32_t i = 0; i < NUMBOIDS; i++) {
		vec_add(&real_center, &(boids[i].pos));
		vec_add(&flocks_avgvel[coreID - 1], &(boids[i].vel));
	}

	sdp_msg_t *udp_msg = spin1_msg_get();

	udp_msg->flags = 0x07;
	udp_msg->tag = 1;

	udp_msg->dest_port = PORT_ETH;
	udp_msg->dest_addr = sv->dbg_addr;

	udp_msg->srce_port = coreID;
	udp_msg->srce_addr = spin1_get_chip_id();

	udp_msg->length = 8; // !! const (sdp_hdr) + data length
	// IMPORTANT: cmd_hdr_t is not used here, so data is written right after the sdp header
	uint8_t *buf = (uint8_t *) &(udp_msg->cmd_rc); // Point at start of command header
	uint16_t increment = NUMBOIDS * (coreID - 1);
	for (uint16_t i = 0; i < NUMBOIDS; i++) {
		boid_move(&boids[i], boids, NUMBOIDS, &real_center,
				&flocks_avgvel[coreID - 1]);
		uint16_t j = i + increment;
		*(buf++) = j;
		*(buf++) = j >> 8;
		*(buf++) = boids[i].pos.x >> 8;
		*(buf++) = boids[i].pos.x >> 16;
		*(buf++) = boids[i].pos.y >> 8;
		*(buf++) = boids[i].pos.y >> 16;
		*(buf++) = boids[i].pos.z >> 8;
		*(buf++) = boids[i].pos.z >> 16;
		udp_msg->length += 8;
		if (udp_msg->length >= BUF_SIZE + 8) {
			//spin1_send_sdp_msg(udp_msg, 33);
			buf = (uint8_t *) &(udp_msg->cmd_rc); // Point at start of command header
			udp_msg->length = 8;
		}
	}
	//If the number of boids is not a multiple of the SDP packet size
	if ( udp_msg->length > 8 ){
		//spin1_send_sdp_msg(udp_msg, 33);
	}
#if 0

	udp_msg->srce_addr = spin1_get_chip_id();
	Vec * buf_vec = (Vec *) &(udp_msg->cmd_rc); // Point at start of command header
	*buf_vec = flocks_avgvel[coreID - 1];
	udp_msg->length = 8 + 12;// !! const (sdp_hdr) + data length
	for (uint16_t i = 1; i <= 16; i++) {
		if (i == coreID)
		continue;
		udp_msg->dest_port = i | (1 << 6); //Port 1
		spin1_send_sdp_msg(udp_msg, 33);
	}
#endif
	spin1_msg_free(udp_msg);
}

void process_sdp(uint m, uint port) {
	sdp_msg_t *msg = (sdp_msg_t *) m;
	Vec * buf_vec = (Vec *) &(msg->cmd_rc); // Point at start of command header
	flocks_avgvel[(msg->srce_port & 0x1F) - 1] = *buf_vec;
}

/****f* main.c/c_main
 *
 * SUMMARY
 * 	main function called by ARC after reset. Initializes timer and registers
 * 	flip_led() as timer callback
 *
 * SYNOPSIS
 * 	-
 *
 * INPUTS
 *	-
 *
 * OUTPUTS
 *	-
 *
 * SOURCE
 */

int c_main() {

	coreID = spin1_get_core_id();
	uint16_t chipID = spin1_get_chip_id();

	uint8_t h = chipID >> 8;
	uint8_t l = chipID & 0xFF;

	for (uint32_t i = 0; i < NUMBOIDS; ++i) {
		new_boid(boids + i, 640, 480);
	}
//	for (uint32_t i = 0; i < sizeof(flocks_avgvel); ++i) {
//		vec_clear(&flocks_avgvel[i]);
//	}

	spin1_delay_us(50000 * coreID);
	io_printf(IO_STD, "Hello! I'm core %u of chip [%u, %u]!\n", coreID, h, l);

	spin1_set_timer_tick(100000);

	spin1_callback_on(TIMER_TICK, update_boids, 3);
//	spin1_callback_on(SDP_PACKET_RX, process_sdp, 2);
	spin1_led_control(LED_ON(1));
	spin1_start();
	return 0;
}

/*
 *******/

