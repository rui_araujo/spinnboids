#ifndef __VEC_H__
#define __VEC_H__
#include "fixedptc.h"

typedef struct {
	fixedpt x, y, z;
} Vec;

void set_vec(Vec * vec, fixedpt X, fixedpt Y, fixedpt Z);
void vec_clear(Vec *vec);
void vec_diff(Vec *vec1, Vec *vec2, Vec *vec3);
void vec_add(Vec *vec1, Vec *vec2);
void vec_smul(Vec *vec, fixedpt scalar);
void vec_sdiv(Vec *vec, fixedpt scalar);
void vec_rshift(Vec *vec, int n);
void vec_lshift(Vec *vec, int n);

/*
 * Limit the length of the longest
 * component to lim, while keeping others
 * in proportion
 */
void vec_limit(Vec *vec, fixedpt lim);

/*
 * Rectangular (ie. min component) distance
 * between this and vec2
 */
fixedpt vec_rdist(Vec *vec1, Vec *vec2);

#endif  /*  __VEC_H__  */
