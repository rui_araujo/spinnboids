#
#       !!!! Do NOT edit this makefile with an editor which replace tabs by spaces !!!!    
#
##############################################################################################
# 
# On command line:
#
# make all = Create project
#
# make clean = Clean project files.
#
# To rebuild project do "make clean" and "make all".
#

##############################################################################################
# Start of default section
#

CC := arm-none-eabi-gcc 
AS := arm-none-eabi-gcc -x assembler-with-cpp
CT := $(CC)
LD := arm-none-eabi-gcc
OC := arm-none-eabi-objcopy
OD := arm-none-eabi-objdump
RM := rm -f
CAT := cat

# Define whether generated code executes in Thumb states
THUMB=no

#
# End of default section
##############################################################################################

##############################################################################################
# Start of user section
#

# Define target name
TARGET = spinnboids

# Define linker script file here
LDSCRIPT = boids.lnk

# List all user directories here
SRCDIR = ./src

# Define build directory here
OBJDIR = ./build

# List all directories to look for include files here
UINCDIR = ./spin_api

# List all additional object files here
UOBJS = 

# List all directories to look for libraries here
ULIBDIR = ./spin_api ./

# List all libraries here (e.g. write "foo" for library "libfoo.a")
ULIBS = spin1_api_gnulib

#
# End of user defines
##############################################################################################


SRC = $(shell find $(SRCDIR) -name \*.c -type f -print) 
ASM = $(shell find $(SRCDIR) -name \*.S -type f -print)
OBJS = $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(patsubst %.c,%.o,$(SRC)))
TRGT = $(OBJDIR)/$(TARGET)

INCDIR  = $(patsubst %,-I %, $(UINCDIR))
LIBDIR  = $(patsubst %,-L %, $(ULIBDIR))
LIBS = $(patsubst %,-l %, $(ULIBS))

CFLAGS=-DDEBUG -O0 -c -Wall -nostdlib -mthumb-interwork \
      -march=armv5te -std=gnu99 $(INCDIR)

ASMFLAGS=-mthumb-interwork -march=armv5te --defsym GNU=1


ifeq ($(THUMB),yes)
  CT += -mthumb -DTHUMB
  AS += --defsym THUMB=1
endif


all: makedir $(OBJS) $(LDSCRIPT)
	$(LD) -T $(LDSCRIPT) $(OBJS) $(UOBJS) $(LIBDIR) $(LIBS)
	$(OC) --set-section-flags APLX=alloc,code,readonly a.out $(TRGT).elf
	$(OC) -O binary -j APLX    $(TRGT).elf APLX.bin
	$(OC) -O binary -j RO_DATA $(TRGT).elf RO_DATA.bin
	$(OC) -O binary -j RW_DATA $(TRGT).elf RW_DATA.bin
	$(OD) -dt $(TRGT).elf > $(TRGT).lst
	${CAT} APLX.bin RO_DATA.bin RW_DATA.bin > $(TRGT).aplx
	${RM} APLX.bin RO_DATA.bin RW_DATA.bin a.out


${OBJDIR}/%.o: ${SRCDIR}/%.c
	$(CC) $(CFLAGS) $< -o $@

makedir:
	echo $(OBJS)
	@mkdir -p ${OBJDIR}

clean:
	${RM}  *.o *.aplx *.elf *.lst *~
	${RM} APLX.bin RO_DATA.bin RW_DATA.bin a.out
	${RM} -f ${OBJ} ${OBJ:.o=.d} ${TARGET}
	${RM} -rf ${OBJDIR}

tidy:
#	${RM} *.o *.elf *.lst *~
#	${RM} APLX.bin RO_DATA.bin RW_DATA.bin a.out
	${RM} -rf ${OBJDIR}
